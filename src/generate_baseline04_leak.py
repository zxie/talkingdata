# -*- coding: utf-8 -*-
"""
Created on Sun Aug 28 15:26:57 2016

@author: Luca
"""
import numpy as np
import keras
import pandas as pd
import os
import sys
import gc
from random import shuffle
from scipy import sparse
from sklearn.preprocessing import LabelEncoder
from sklearn.cross_validation import train_test_split
from sklearn.metrics import log_loss

from sklearn.datasets import load_svmlight_file

from keras.layers.advanced_activations import PReLU
from keras.models import Sequential
from keras.layers import Dense, Dropout, Activation
from keras.wrappers.scikit_learn import KerasClassifier
from keras.utils import np_utils
from keras.optimizers import SGD
from keras.callbacks import EarlyStopping

#------------------------------------------------- Parameters ----------------------------------------

resamples  = 10
ROW_SAMPLE = 0.98
COL_SAMPLE = 0.90
BATCH = 800
submission = 'baseline04_leak' 

#------------------------------------------------- Write functions ----------------------------------------

def rstr(df): return df.dtypes, df.head(3) ,df.apply(lambda x: [x.unique()]), df.apply(lambda x: [len(x.unique())]),df.shape

def batch_generator(X, y, batch_size, shuffle):
    #chenglong code for fiting from generator (https://www.kaggle.com/c/talkingdata-mobile-user-demographics/forums/t/22567/neural-network-for-sparse-matrices)
    number_of_batches = np.ceil(X.shape[0]/batch_size)
    counter = 0
    sample_index = np.arange(X.shape[0])
    if shuffle:
        np.random.shuffle(sample_index)
    while True:
        batch_index = sample_index[batch_size*counter:batch_size*(counter+1)]
        X_batch = X[batch_index,:].toarray()
        y_batch = y[batch_index]
        counter += 1
        yield X_batch, y_batch
        if (counter == number_of_batches):
            if shuffle:
                np.random.shuffle(sample_index)
            counter = 0

def batch_generatorp(X, batch_size, shuffle):
    number_of_batches = X.shape[0] / np.ceil(X.shape[0]/batch_size)
    counter = 0
    sample_index = np.arange(X.shape[0])
    while True:
        batch_index = sample_index[batch_size * counter:batch_size * (counter + 1)]
        X_batch = X[batch_index, :].toarray()
        counter += 1
        yield X_batch
        if (counter == number_of_batches):
            counter = 0

def baseline_model():
    # create model
    model = Sequential()
    model.add(Dense(128, input_dim=X_train.shape[1], init='normal'))
    model.add(PReLU())
    model.add(Dropout(0.4))
    model.add(Dense(48, input_dim=X_train.shape[1], init='normal'))
    model.add(PReLU())
    model.add(Dropout(0.3))
    model.add(Dense(12, init='normal', activation='softmax'))
    # Compile model
    model.compile(loss='sparse_categorical_crossentropy', optimizer='adadelta', metrics=['accuracy'])  #logloss
    return model

#------------------------------------------------ Read data from source files ------------------------------------

if __name__ == '__main__':
    seed = 101
    np.random.seed(seed)
    datadir = 'input'
    
    print("### ----- PART 1 ----- ###")
            
    ##################
    #  Train and Test
    ##################
    print("# Recover Train and Test")
    
    
    
    train_sp, Y = load_svmlight_file(os.path.join(datadir,'svmlight_train.csv'))
    test_sp,_ = load_svmlight_file(os.path.join(datadir,'svmlight_test.csv'))
    
    features = range(train_sp.shape[1])
    limit = int(train_sp.shape[1]*COL_SAMPLE)
    
    
    with open('cv_id.txt','rb') as R:
        cv_prg = map(lambda x: int(x.strip()), R.readlines())
    
    cv_seq = [list() for i in range(5)]
    for n,v in enumerate(cv_prg):
        cv_seq[v-1].append(n)
    
    validation_scores = list()
    cv_predictions = np.zeros((train_sp.shape[0],12))
    
    rounds = list()
    
    for iterations in range(resamples):
        
        shuffle(features)
        
        for cv in range(5):
            print 'CV fold %i' % (cv+1)
            insample  = [item for k,sublist in enumerate(cv_seq) if k != cv for item in sublist]
            subsample = int(len(insample) * ROW_SAMPLE)
            print 'subsampling %i examples' % subsample
            outsample = cv_seq[cv]
            shuffle(insample)
            X_train, X_val, y_train, y_val = train_sp[insample[:subsample],:][:,features[:limit]], train_sp[outsample,:][:,features[:limit]], Y[insample[:subsample]], Y[outsample]
            
            ##################
            #  Build Model
            ##################
            
            print("# Num of Features: ", X_train.shape[1])
            
            model=baseline_model()
            early_stopping = EarlyStopping(monitor='val_loss', patience=3, verbose=2)
            fit= model.fit_generator(generator=batch_generator(X_train, y_train, BATCH, True),
                                     nb_epoch=50,
                                     samples_per_epoch=X_train.shape[0],
                                     validation_data=(X_val.todense(), y_val), verbose=2,
                                     callbacks=[early_stopping]
                                     )
            rounds.append(max(fit.epoch))
            # evaluate the model
            cv_predictions[outsample] = cv_predictions[outsample]+model.predict_generator(generator=batch_generatorp(X_val, BATCH, False), val_samples=X_val.shape[0])
            ll_score = log_loss(y_val, cv_predictions[outsample])
            validation_scores.append(ll_score)
            print('logloss val {}'.format(ll_score))
        
        print('Expected logloss val {}'.format(np.mean(validation_scores)))
        
        print("# Averaged prediction")
        insample = range(train_sp.shape[0])
        subsample = int(len(insample) * 0.80)
        shuffle(insample)
        early_stopping = EarlyStopping(monitor='val_loss', patience=3, verbose=2)
        fit= model.fit_generator(generator=batch_generator(train_sp[insample[:subsample],:][:,features[:limit]], Y[insample[:subsample]], BATCH, True),
                                         nb_epoch=int(np.mean(rounds)),
                                         samples_per_epoch=X_train.shape[0],
                                         validation_data=(train_sp[insample[subsample:],:][:,features[:limit]].todense(), Y[insample[subsample:]]), 
                                         verbose=2, 
                                         callbacks=[early_stopping]
                                         )
        if iterations==0:
            scores = model.predict_generator(generator=batch_generatorp(test_sp[:,features[:limit]], BATCH, False), val_samples=test_sp.shape[0])
        else:
            scores += model.predict_generator(generator=batch_generatorp(test_sp[:,features[:limit]], BATCH, False), val_samples=test_sp.shape[0])
    
    # Averaging of all results
    cv_predictions = cv_predictions / float(resamples)
    result = pd.DataFrame(scores / (float(resamples) * (float(cv)+1.0)) , columns=['F23-','F24-26','F27-28','F29-32','F33-42','F43+','M22-','M23-26','M27-28','M29-31','M32-38','M39+'])
    device_id = pd.read_csv('sample_submission.csv')
    result["device_id"] = device_id.device_id
    print(result.head(1))
    result = result.set_index("device_id")
    
    ##################
    #  Saving Results
    ##################
    from sklearn.datasets import dump_svmlight_file
    # Metric
    with open('metric/'+submission+'.val','wb') as W:
        W.write(submission+'\t'+str(np.mean(validation_scores))+'\n')
    # Test predictions
    result.to_csv('tst/'+submission+'.csv', index=True, index_label='device_id')
    # Validation predictions
    np.savetxt('val/'+submission+'.val.yht', cv_predictions, delimiter=',')
    # Datasets
    dump_svmlight_file(X=train_sp, y=Y, f='feature/'+submission+'.trn.sps')
    dump_svmlight_file(X=test_sp, y=np.zeros(test_sp.shape[0]), f='feature/'+submission+'.tst.sps')
    
    print("Done")




