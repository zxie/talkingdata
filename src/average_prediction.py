#!/usr/bin/env python
from __future__ import division
import argparse
import numpy as np


if __name__ == '__main__':
    parser = argparse.ArgumentParser()
    parser.add_argument('--valid-predictions', required=True, nargs='+', dest='valid_predictions')
    parser.add_argument('--test-predictions', required=True, nargs='+', dest='test_predictions')
    parser.add_argument('--predict-valid-file', required=True, dest='predict_valid_file')
    parser.add_argument('--predict-test-file', required=True, dest='predict_test_file')
    args = parser.parse_args()

    for i, (valid_prediction, test_prediction) in enumerate(zip(args.valid_predictions,
                                                                args.test_predictions), 1):
        if i == 1:
            P_val = np.loadtxt(valid_prediction, delimiter=',')
            P_tst = np.loadtxt(test_prediction, delimiter=',')
        else:
            P_val += np.loadtxt(valid_prediction, delimiter=',')
            P_tst += np.loadtxt(test_prediction, delimiter=',')

    P_val /= i
    P_tst /= i

    P_val = (P_val.T / P_val.sum(axis=1)).T
    P_tst = (P_tst.T / P_tst.sum(axis=1)).T

    np.savetxt(args.predict_valid_file, P_val, delimiter=',')
    np.savetxt(args.predict_test_file, P_tst, delimiter=',')
