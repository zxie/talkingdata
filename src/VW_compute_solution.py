# -*- coding: utf-8 -*-
"""
Created on Tue Aug 23 00:15:43 2016

@author: Luca
"""
import numpy as np
import pandas as pd
import math

import subprocess
from random import shuffle
from sklearn.metrics import log_loss

filename = 'h4'
# --lrqfa FF2 --passes=50
# --passes=50
train_params = filename+'.shuffle.trn.vw -f '+filename+'.model --oaa 12 --loss_function logistic -k --cache_file cache_train.vw --passes=50 --ftrl --holdout_off -b 21 -l 0.4 --l1 0.0000001 --l2 0.000001'

def sigmoid(x): 
    return 1 / (1 + math.exp(-x))
    
def execute_vw(parameters, silent=False):
    execution = subprocess.Popen('vw '+parameters, \
    shell=True, stderr=subprocess.PIPE)
    line = ""
    history = ""
    while True:
        out = execution.stderr.read(1)
        history += out
        if out == '' and execution.poll() != None:
            if not silent:
                print '------------ COMPLETED ------------\n'
            break
        if out != '':
            line += out
            if '\n' in line[-2:]:
                if not silent:
                    print line[:-2]
                line = ''
    return history.split('\r\n')

if __name__ == "__main__":
    
    with open('cv_id.txt','rb') as R:
        cv_prg = map(lambda x: int(x.strip()), R.readlines())
    
    cv_seq = [list() for i in range(5)]
    for n,v in enumerate(cv_prg):
        cv_seq[v-1].append(n)

    index = list()
    cv_score = list()
    for cv in range(5):
        outofsample = {idx:True for idx in cv_seq[cv]}
        cv_prediction = list()
        
        with open(filename+'.trn.vw','rb') as R:
            train      = list()
            validation = list()
            val_sol = list()
            for n, line in enumerate(R):
                if n in outofsample:
                    validation.append(line)
                    val_sol.append(int(line.split()[0])-1)
                else:
                    train.append(line)
            shuffle(train)
        with open(filename+'.shuffle.trn.vw','wb') as W:
            for element in train:
                W.write(element)
        with open(filename+'.val.trn.vw','wb') as W:
            for element in validation:
                W.write(element)
                
        results = execute_vw(train_params)
        
        params = '-t '+filename+'.val.trn.vw -i '+filename+'.model -k -r pred_'+filename+'.test'
        results = execute_vw(params, silent=True)
        
        with open('pred_'+filename+'.test','rb') as R:
            preds = R.readlines()
    
        
        predictions = []    
        for line in preds:
            data = line.strip().split()
            index.append(int(data[-1]))
            values = [math.exp(float(c.split(':')[1])) for c in data[:-1]]
            values = [v / sum(values)  for v in values]
            predictions.append(values)
        
        ll_score = log_loss(val_sol, predictions)
        print 'Validation logloss: %0.5f\n' % ll_score
        cv_score.append(ll_score)
        if cv==0:
            cv_predictions = pd.DataFrame(predictions)
        else:
            cv_predictions = cv_predictions.append(predictions)
        
        cv_predictions['row'] = index
        cv_predictions = cv_predictions.sort_values(by='row')
        
        cv_predictions.drop('row',axis=1).to_csv(filename.upper()+'_TRAIN.val', index=False, header=False)
        
    print 'Average validation logloss: %0.5f\n' % np.mean(cv_score)
    
    with open(filename+'.trn.vw','rb') as R:
            train      = list()
            for n, line in enumerate(R):
                train.append(line)
            shuffle(train)
    with open(filename+'.shuffle.trn.vw','wb') as W:
            for element in train:
                W.write(element)
    
    results = execute_vw(train_params)
    
    params = '-t '+filename+'.tst.vw -i '+filename+'.model -k -r pred_'+filename+'.test'
    results = execute_vw(params, silent=False)
    
    with open('pred_'+filename+'.test','rb') as R:
        preds = R.readlines()
    
    predictions = []
    index = []
    for n,line in enumerate(preds):
        data = line.strip().split()
        index.append(int(data[-1]))
        values = [math.exp(float(c.split(':')[1])) for c in data[:-1]]
        values = [v / sum(values)  for v in values]
        predictions.append(values)
    
    predictions = pd.DataFrame(predictions)
    predictions.columns = ['F23-','F24-26','F27-28','F29-32','F33-42','F43+','M22-','M23-26','M27-28','M29-31','M32-38','M39+']
    predictions['device_id'] = index
    predictions[['device_id','F23-','F24-26','F27-28','F29-32','F33-42','F43+','M22-','M23-26','M27-28','M29-31','M32-38','M39+']].to_csv(filename.upper()+'_TEST.sub', index=False)
    
    with open(filename.upper()+'_SCORE.val', 'wb') as W:
        W.write(train_params + ' ' +str(np.mean(cv_score)))
    