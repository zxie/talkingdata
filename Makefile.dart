include Makefile.feature.h2

N = 10000
DEPTH = 6
LRATE = 0.05
SUBCOL = 0.5
SUBROW = 0.8
SUBLEV = 0.5
WEIGHT = 1
DROP = 0.1
SKIP = 0.5
N_STOP = 100
ALGO_NAME := dart_$(N)_$(DEPTH)_$(LRATE)_$(SUBCOL)_$(SUBROW)_$(SUBLEV)_$(WEIGHT)_$(DROP)_$(SKIP)_$(N_STOP)
MODEL_NAME := $(ALGO_NAME)_$(FEATURE_NAME)

METRIC_VAL := $(DIR_METRIC)/$(MODEL_NAME).val.txt

PREDICT_VAL := $(DIR_VAL)/$(MODEL_NAME).val.yht
PREDICT_TST := $(DIR_TST)/$(MODEL_NAME).tst.yht

SUBMISSION_TST := $(DIR_TST)/$(MODEL_NAME).sub.csv
SUBMISSION_TST_GZ := $(DIR_TST)/$(MODEL_NAME).sub.csv.gz

all: validation submission
validation: $(METRIC_VAL)
submission: $(SUBMISSION_TST)
retrain: clean_$(ALGO_NAME) submission

$(PREDICT_TST) $(PREDICT_VAL): $(FEATURE_TRN) $(FEATURE_TST) $(CV_ID) \
                                   | $(DIR_VAL) $(DIR_TST)
	./src/train_predict_dart.py --train-file $< \
                              --test-file $(word 2, $^) \
                              --predict-valid-file $(PREDICT_VAL) \
                              --predict-test-file $(PREDICT_TST) \
                              --depth $(DEPTH) \
                              --lrate $(LRATE) \
                              --n-est $(N) \
                              --subcol $(SUBCOL) \
                              --subrow $(SUBROW) \
                              --sublev $(SUBLEV) \
                              --weight $(WEIGHT) \
                              --drop $(DROP) \
                              --skip $(SKIP) \
                              --early-stop $(N_STOP) \
                              --cv-id $(lastword $^)

$(SUBMISSION_TST_GZ): $(SUBMISSION_TST)
	gzip $<

$(SUBMISSION_TST): $(PREDICT_TST) $(HEADER) $(ID_TST) | $(DIR_TST)
	paste -d, $(lastword $^) $< > $@.tmp
	cat $(word 2, $^) $@.tmp > $@
	rm $@.tmp

$(METRIC_VAL): $(PREDICT_VAL) $(Y_TRN) | $(DIR_METRIC)
	python ./src/evaluate.py --predict-file $< \
                             --target-file $(word 2, $^) > $@
	cat $@


clean:: clean_$(ALGO_NAME)

clean_$(ALGO_NAME):
	-rm $(METRIC_VAL) $(PREDICT_VAL) $(PREDICT_TST) $(SUBMISSION_TST)
	find . -name '*.pyc' -delete

.DEFAULT_GOAL := all
